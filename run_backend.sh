#!/bin/sh

source backend/venv/bin/activate
FLASK_ENV=development python $(pwd)/backend/run.py
