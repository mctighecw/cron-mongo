# MongoDB Reference: Backup and Restore

- Dump db (all collections)

```
mongodump -h localhost:27017 -u admin -p password --authenticationDatabase admin -d addressbook
```

- Dump db (only a specific collection)

```
mongodump -h localhost:27017 -u admin -p password --authenticationDatabase admin -d addressbook -c friends
```

- Archive backup

```
# To a folder with archived files
mongodump -h localhost:27017 -u admin -p password --authenticationDatabase admin -d addressbook --gzip -o ./backup1

# To an archive file
mongodump -h localhost:27017 -u admin -p password --authenticationDatabase admin -d addressbook --gzip --archive=backup1.gz
```

- Full dump (all dbs) with operation log

```
mongodump -h localhost:27017 -u admin -p password --authenticationDatabase admin --oplog
```

- Clone a database (make a copy with another name)

```
mongodump -h localhost:27017 -u admin -p password --authenticationDatabase admin -d addressbook --archive | mongorestore -h localhost:27017 -u admin -p password --authenticationDatabase admin --archive  --nsFrom='addressbook.*' --nsTo='addressbook2.*'
```

- Restore db

```
# To the same database as before
mongorestore -h localhost:27017 -u admin -p password --authenticationDatabase admin --gzip --archive=addressbook-2020-01-30_16-22.gz

# To a new database (clone)
mongorestore -h localhost:27017 -u admin -p password --authenticationDatabase admin --gzip --archive=addressbook-2020-01-30_16-22.gz --nsFrom='addressbook.*' --nsTo='addressbook2.*'
```
