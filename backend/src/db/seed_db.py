import os
import sys

root_dir = os.path.abspath(os.curdir)
sys.path.append(root_dir)

from src.models import Friend
from seeds import friends_data

def seed_db():
    """
    Seeds database with initial data.
    """
    for item in friends_data:
        print(item['first_name'])

        for key in item:
            friend = Friend(**item)

        friend.save()
