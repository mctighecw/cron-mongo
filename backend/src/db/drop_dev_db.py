import pymongo

client = pymongo.MongoClient(
    'localhost',
    port=27017,
    username='admin',
    password='mypassword',
    authSource='admin')

print('Dropping database collections...')

db = client.get_database('addressbook')
friends = db.friends

friends.drop()
