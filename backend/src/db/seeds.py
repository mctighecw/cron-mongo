friends_data = [
    {
        'first_name': 'Susan',
        'last_name': 'Williams',
        'address': '10 Main Street',
        'city': 'Springfield',
        'state': 'MA',
        'zip': '12345',
        'country': 'U.S.A.',
        'email': 'susan@williams.com',
        'tel_nr': '(555)-123-4567'
    },
    {
        'first_name': 'Sam',
        'last_name': 'Jones',
        'address': '14 Elm Lane',
        'city': 'Youngtown',
        'state': 'PA',
        'zip': '87654',
        'country': 'U.S.A.',
        'email': 'sam@jones.com',
        'tel_nr': '(555)-654-3210'
    },
    {
        'first_name': 'Anna',
        'last_name': 'McMahon',
        'address': '25 Oak Avenue',
        'city': 'Darrytown',
        'state': 'New Hampshire',
        'zip': '90123',
        'country': 'U.S.A.',
        'email': 'anna@mcmahon.com',
        'tel_nr': '(555)-321-0987'
    }
]
