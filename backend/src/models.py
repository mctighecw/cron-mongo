from datetime import datetime, timezone

from mongoengine import Document
from mongoengine.fields import (StringField, ListField, DateTimeField)


class Friend(Document):
    meta = { 'collection': 'friends' }

    first_name = StringField()
    last_name = StringField()
    address = StringField()
    city = StringField()
    state = StringField()
    zip = StringField()
    country = StringField()
    email = StringField()
    tel_nr = StringField()
    updated_at = DateTimeField(default=datetime.now(timezone.utc).astimezone())
