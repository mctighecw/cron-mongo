import logging
import os

from flask import Flask
from flask_cors import CORS
from flask_mongoengine import MongoEngine

from src import flask_config
from src.routes.internal import internal

def create_app():
    app = Flask(__name__)
    app.config.from_object(flask_config)

    FLASK_ENV = os.getenv("FLASK_ENV")

    if FLASK_ENV == "development":
        CORS(app)

    logging.basicConfig(format="%(asctime)s %(levelname)s [%(module)s %(lineno)d] %(message)s",
                        level=app.config["LOG_LEVEL"])

    db = MongoEngine(app)

    app.register_blueprint(internal, url_prefix="/api/internal")

    return app
