import os
import time
import logging
import gzip
import boto3
from datetime import datetime
from sh import mongodump
from botocore.exceptions import ClientError

from src.env_config import (DB_HOST_DEV, DB_HOST_PROD, DB_NAME, DB_AUTH,
    DB_USER, DB_PASSWORD, AWS_BUCKET, AWS_ACCESS_KEY, AWS_SECRET_KEY)

FLASK_ENV = os.getenv("FLASK_ENV")

if FLASK_ENV == "development":
    DB_HOST = DB_HOST_DEV
else:
    DB_HOST = DB_HOST_PROD


def backup_db():
    time_now = datetime.utcnow()
    date_time_file = time_now.strftime("%Y-%m-%d_%H-%M")
    date_time_email = time_now.strftime("%Y-%m-%d, %H:%M")

    root_dir = os.path.abspath(os.curdir)
    file_name = f"{DB_NAME}-{date_time_file}.gz"
    file_path = os.path.join(root_dir, "temp", file_name)

    try:
        mongodump("-h", DB_HOST, "-u", DB_USER, "-p", DB_PASSWORD,
            "--authenticationDatabase", DB_AUTH, "-d", DB_NAME,
            "--gzip", f"--archive={file_path}")

        time.sleep(3)

        if os.stat(file_path).st_size != 0:
            success = upload_backup(file_path, file_name)
            return DB_NAME, date_time_email, success
        else:
            print("Error, file is empty")
            return DB_NAME, date_time_email, False
    except:
        print("Error dumping database")
        return DB_NAME, date_time_email, False


def upload_backup(file_path, file_name):
    s3 = boto3.client(
        "s3",
        aws_access_key_id=AWS_ACCESS_KEY,
        aws_secret_access_key=AWS_SECRET_KEY,
    )

    try:
        s3.upload_file(file_path, AWS_BUCKET, file_name)
        return True
    except ClientError as e:
        logging.error(e)
        return False
    finally:
        os.remove(file_path)
