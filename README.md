# README

This is a template for automating MongoDB database backups to the cloud.

There is a Python Flask backend connected to MongoDB, which has some sample data. The _cron_ service makes an API call to the backend at regular intervals, which dumps the database into a _gzip_ file. This file, which has a custom filename made up of the database name along with a date/time stamp, is then uploaded to a private bucket on [AWS S3](https://aws.amazon.com/s3). Finally, a status email is sent to the user.

## App Information

App Name: cron-mongo

Created: January 2020

Creator: Christian McTighe

Email: mctighecw@gmail.com

Repository: [Link](https://gitlab.com/mctighecw/cron-mongo)

## Tech Stack

- Python 3.6
- Flask
- MongoDB
- _gzip_
- _cron_ (Linux job scheduler)
- Alpine Linux
- AWS S3
- Gmail
- Docker

## Setup and Run via Docker

1. Make AWS account, set up S3 bucket, set user permissions
2. Make Gmail account and turn on Google "less secure app access"
3. Add .env file with necessary variables to project root
4. Run commands:

```
$ cd cron-mongo
$ source build.sh
```

5. Add admin user with rights

```
docker exec -it cron-mongo_mongodb_1 bash
mongo
use admin

db.createUser(
  {
    user: "admin",
    pwd: passwordPrompt(),
    roles: [
      "userAdminAnyDatabase",
      "readWriteAnyDatabase",
      "backup",
      "restore"
    ]
  }
)

exit
mongo -u admin --authenticationDatabase admin -p
use admin
db.getUser("admin")
exit
```

6. Run command:

```
$ source init_docker_db.sh
```

## More Information

See `mongo_reference.md` for more on backing up and restoring.

Last updated: 2025-02-03
